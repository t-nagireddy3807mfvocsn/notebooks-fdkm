# README #

This repo contains several notebooks demonstrating the implementation of various Machine Learning models.

The "notebook view" add-on has been enabled. Please make sure to change the viewer from the **"Default File Viewer"** to **"Ipython Notebook"**.

Each notebook contains a description of the respective anaylsis, and it's results.